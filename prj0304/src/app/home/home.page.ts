import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo ="Ímpetos Motos";

  logo="../../../assets/images/logo.png"
  cards = [
 { 
  titulo:"Biz 125 Rosa ",
  subti: "a nova Honda Biz 125 é uma evolução da espécie. De uma ponta a outra, o modelo 2011 da cub recebeu pequenas mudanças que a deixam ainda mais competitiva e à frente das concorrentes. Do escudo frontal à lanterna traseira, tudo é novo, assim como o banco e a posição de pilotagem. O motor agora funciona com álcool ou gasolina e o tanque de combustível cresceu. São 5,5 litros que aumentam a autonomia, um dos pontos fracos do modelo.",
  conteudo:"",
  foto:"http://2.bp.blogspot.com/_jHubvjtBJW8/TVFsaNtkB3I/AAAAAAAABDw/rqUdH0lVgJk/s1600/Biz+Rosa+2011.jpg"
  },
  {
    titulo: "Honda CB 500F",
    subti: "Tudo na CB 500F foi modernizado. Há poucas peças expostas e só são visíveis os itens importantes para uma naked. Todo o resto é coberto por algum acabamento. Grandes painéis cobrem toda lateral da moto, até a coroa da roda traseira tem uma cobertura adicional na capa da corrente para cobrir aquela parte sempre suja da moto. As grandes abas laterais ao tanque tem uma cobertura interna que contorna tudo pela parte de dentro, dirigindo o ar diretamente ao radiador. Se olhar pela frente, esse desenho ficou bastante agressivo, dando ao farol linhas atuais e também grande capacidade de iluminação e visibilidade.",
    conteudo:"",
    foto:"https://images.caradisiac.com/images/6/6/5/4/156654/S0-essai-honda-cb-500-f-2016-details-et-portfolio-27-photos-510008.jpg"
  },
  {
    titulo: "NXR Bros 150",
    subti: "Especificação técnica da moto NXR 150 Bros, Trail da Honda, com motor 4 tempos, arrefecido a ar, OHC, monocilíndrico, acionado por corrente, 2 válvulas, e 149,2 CC",
    conteudo: "",
    foto:"http://1.bp.blogspot.com/-4ujP1snm1Eg/TmJYQ0GVvYI/AAAAAAAAAP4/qGS1fglgUZ4/s1600/NXR-150-Bros-%25C3%2587-a-%25C2%25A3nica-motocicleta-da-sua-categoria-com-tecnologia-Flex_2.jpg"
  },
  {
    titulo: "Ducati Hypermotard ",
    subti: "A Ducati Hypermotard é uma motocicleta Ducati supermotard projetada por Pierre Terblanche. A Hypermotard é equipada com o motor 'Desmo' ou 'Desmodromic' em 90 ° V. Além disso, a motocicleta atinge as velocidades de 201 km/h.",
    conteudo: "",
    foto:"https://veja.abril.com.br/wp-content/uploads/2016/06/esporte-motos-ducati-hypermotard-20121023-001-original.jpeg?quality=70&strip=info&w=928"
  },
  {
    titulo: "R1250 GB ",
    subti: "A nova BMW R 1250 GS chega com diversas novidades, como o motor boxer de última geração, aumento de cilindrada para 1.254 cc e a presença do sistema ShiftCam, que melhora a entrega do motor em todas as faixas de rotação. Potência e torque também aumentaram. A R 1250 GS agora tem 136 cv de potência máxima a 7.750 rpm, além de um torque de 14,5 kgf.m a 6.250 rpm. A versatilidade e o ótimo desempenho em todos os tipos de terrenos permanecem presentes. ",
    conteudo: "",
    foto:"https://i0.statig.com.br/bancodeimagens/dx/md/02/dxmd02hhbhfwg635iwvizjftg.jpg"
  },
  {
    titulo: "XT 600",
    subti: "A Yamaha XT600 é derivada da Yamaha XT550, produzida de 1978 a 1984. A XT600 começou sua produção em 1984, seguindo com a XT600E, em 1999, para o ano modelo 2000. Os dois modelos XT600 são robustos, para estrada, e adaptam-se bem à condução urbana. A XT600 e a XT600E apresentam um motor monocilíndrico de 595cc e pneus de uso duplo. A XT600 e a XT600E competem com a Honda XR600R e NX650 Dominator e com a Suzuki DR650.",
    conteudo: "",
    foto:"https://tse4.mm.bing.net/th?id=OIP.bHiJd28HLNja7lcJ8iLrmAHaF_&pid=Api&P=0&w=203&h=164"
  },
  {
    titulo: "XRE 300 2018",
    subti: "Moderna e com estilo próprio, a XRE 300 conta com tampa articulada ao tanque que controla a evaporação e minimiza o desperdício. Além disso, a tampa articulada garante praticidade ao abastecer.Na XRE 300 você tem um painel black-out 100% digital, que proporciona uma excelente visualização e leitura rápida das informações, facilitando a pilotagem. Funcional e moderno, o painel conta com velocímetro, hodômetro parcial e total, marcador de nível de combustível, conta-giros, relógio e luzes-espia.",
    conteudo: "",
    foto:"https://i1.wp.com/s2.glbimg.com/H4bis_aaAuRpJDOHZNGl0oOuY-g=/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2017/f/g/7DH18kSCWd6r4a35jZEg/xre-300-std-adventure-lateral.jpg?ssl=1"
  },
  ];
 
  constructor() {}

}
